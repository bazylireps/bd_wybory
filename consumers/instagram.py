from kafka import KafkaConsumer
from parsers.instagram import parse_tag, parse_user
from utils.hdfs_utils import write_to_hdfs
import json
import time

'''
Andrzej Duda -         
Kidawa -         mkblonska
Kosiniak-Kamysz -     kosiniakkamysz
Hołownia  -         szymon.holownia
Bosak -         krzysztofbosak
Biedroń -         robertbiedron
żółtek stanisław -     brak?
Jakubiak -         jakubiakmarek
Piotrkowski -         brak?
Tanajno -         tanajno
Trzaskowski -         trzaskowskirafal
'''


def get_candidate_by_user(nametag):
    if nametag == 'prezydent_pl' or nametag == 'andrzejduda':
        return 'duda'


def get_candidate_by_tag(nametag):
    if nametag == 'andrzejduda':
        return 'duda'


def create_path(message_key, username, ts):
    hdfs_path = f'user/hadoop/projekt/{username}/instagram/{ts}_{message_key}'
    return hdfs_path


def instagram_consumer():
    consumer = KafkaConsumer('instagram',
                             group_id='my-group',
                             bootstrap_servers=['localhost:9092'],
                             auto_offset_reset='latest')
    for message in consumer:
        # message value and key are raw bytes -- decode if necessary!
        # e.g., for unicode: `message.value.decode('utf-8')`
        time.sleep(1)
        timestamp = time.time()
        key = message.key.decode('utf-8')
        val = json.loads(message.value.decode('utf-8'))
        if key == 'user':
            data = parse_user(val)
            username = get_candidate_by_user(data['username'])
        else:
            data = parse_tag(val)
            username = get_candidate_by_tag(data['tag_name'])
        data = json.dumps(data)
        pth = create_path(key, username, timestamp)
        write_to_hdfs(pth, data)
