import requests
from kafka import KafkaProducer
import json

user_url = "prezydent_pl"
tag_url = "andrzejduda"

'''
Andrzej Duda -         
Kidawa -         mkblonska
Kosiniak-Kamysz -     kosiniakkamysz
Hołownia  -         szymon.holownia
Bosak -         krzysztofbosak
Biedroń -         robertbiedron
żółtek stanisław -     brak?
Jakubiak -         jakubiakmarek
Piotrkowski -         brak?
Tanajno -         tanajno
Trzaskowski -         trzaskowskirafal
'''


def produce_user(username_do_url):
    producer = KafkaProducer(bootstrap_servers=['localhost:9092'])
    url = "https://www.instagram.com/" + username_do_url + "/?__a=1"
    r = requests.get(url)
    data = json.dumps(r.json())
    producer.send('instagram', bytearray(data.encode('utf-8')), key=b'user')


def produce_tags(tag_do_url):
    producer = KafkaProducer(bootstrap_servers=['localhost:9092'])
    tag_url = "https://www.instagram.com/explore/tags/" + tag_do_url + "/?__a=1"
    r = requests.get(tag_url)
    data = json.dumps(r.json())
    producer.send('instagram', bytearray(data.encode('utf-8')), key=b'tag')


produce_tags(tag_url)
produce_user(user_url)
