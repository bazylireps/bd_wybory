import requests

main_url = 'http://192.168.0.51:9870'
user_url = 'user.name=hadoop'


def write_to_hdfs(path, data):
    temp_url = f'{main_url}/webhdfs/v1/{path}?{user_url}&op=CREATE'
    resp = requests.put(temp_url, allow_redirects=False)
    redirect_url = resp.headers['Location']
    resp2 = requests.put(redirect_url, data=data, allow_redirects=False)
    print(f'status code: {resp2.status_code}, reason: {resp2.reason}')
    return resp2.status_code


def read_from_hdfs(path):
    temp_url = f'{main_url}/webhdfs/v1/{path}?{user_url}&op=OPEN'
    resp = requests.get(temp_url, allow_redirects=False)
    redirect_url = resp.headers['Location']
    resp2 = requests.get(redirect_url, allow_redirects=False)
    print(f'status code: {resp2.status_code}, reason: {resp2.reason}')
    print(resp2.content)
