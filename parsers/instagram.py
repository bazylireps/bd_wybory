import requests
import json


def parse_user(data):
    output_data = {}
    output_data['username'] = data['graphql']['user']['username']
    output_data['user_id'] = data['graphql']['user']['id']
    output_data['followers'] = data['graphql']['user']['edge_followed_by']['count']
    output_data['posts_number'] = data['graphql']['user']['edge_owner_to_timeline_media']['count']

    posts = []
    for x in data['graphql']['user']['edge_owner_to_timeline_media']['edges']:
        post_id = x['node']['id']
        comments = x['node']['edge_media_to_comment']['count']
        likes = x['node']['edge_liked_by']['count']
        is_video = x['node']['is_video']
        views = 'not a video'
        if is_video:
            views = x['node']['video_view_count']

        posts.append({
            'postId': post_id,
            'comments': comments,
            'likes': likes,
            'isVideo': is_video,
            'views': views
        })
    output_data['posts'] = posts
    return output_data


def parse_tag(data):
    tag_name = data['graphql']['hashtag']['name']
    tag_id = data['graphql']['hashtag']['id']
    tag_count = data['graphql']['hashtag']['edge_hashtag_to_media']['count']
    return {'tag_name': tag_name, 'tag_id': tag_id, 'tag_count': tag_count}


if __name__ == '__main__':
    usernameDoUrl = "prezydent_pl"
    url = "https://www.instagram.com/" + usernameDoUrl + "/?__a=1"
    r = requests.get(url)
    data = r.json()
    res = parse_user(data)
    print(res)

    tagDoUrl = "prezydent"
    tag_url = "https://www.instagram.com/explore/tags/" + tagDoUrl + "/?__a=1"
    r = requests.get(tag_url)
    data = r.json()
    res = parse_tag(data)
    print(res)
